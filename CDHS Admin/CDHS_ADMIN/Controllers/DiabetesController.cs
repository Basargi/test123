﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CDHS_ADMIN.Models;
using System.Globalization;
using System.Web.Hosting;
using System.IO;

namespace CDHS_ADMIN.Controllers
{
    public class DiabetesController : Controller
    {
        private cdhsCommonMethods _cdhsCommonMethods = new cdhsCommonMethods();
        //
        // GET: /Diabetes/
        //public ActionResult adddiabetescontent()
        //{

        //}

        public ActionResult diabetesgraph()
        {
            return View();
        }
        public ActionResult diabetesreportuserdetail()
        {
            return View();
        }
        public ActionResult diabetesreport()
        {
            DiabetesReportViewModel _DiabetesReportViewModel = new DiabetesReportViewModel();
            _DiabetesReportViewModel.StateList = new List<StateListModel>();

            _DiabetesReportViewModel.CityList = new List<CityListModel>();

            _DiabetesReportViewModel.AgeList = new List<AgeListModel>();
            return View(_DiabetesReportViewModel);
        }

        #region Auto Complete Diabetes Content
        public ActionResult AutocompleteDiabetesContent(string searchstring) {
            JsonResult strSearch = _cdhsCommonMethods.ContentTypeGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Auto Complete Diabetes Category
        public ActionResult AutocompleteDiabetesCategory(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentCategoryGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Auto Complete Diabetes Company
        public ActionResult AutocompleteDiabetesCompany(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentCompanyGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Auto Complete Diabetes Symptoms Sponsors Name
        public ActionResult AutocompleteDiabetesSymptoms(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentSymptomsGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult viewdiary()
        {
            return View();
        }

        #region DiabetesContent_cdhs
        public ActionResult viewdiabetescontent()
        {
            IEnumerable<Diabetes_Content> _Diabetes_Content = _cdhsCommonMethods.GetDiabetesContentcdhsData();
            ViewDiabetesContentViewModel _ViewDiabetesContentViewModel = new ViewDiabetesContentViewModel();
            // Action list for temprory use delete it later
            _ViewDiabetesContentViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewDiabetesContentViewModel.diabetescontentcdhs = _Diabetes_Content;

            return View(_ViewDiabetesContentViewModel);
        }
        [HttpPost]
        public ActionResult viewdiabetescontent(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("adddiabetescontent", "Diabetes", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewdiabetescontent", "Diabetes");
        }
        public ActionResult adddiabetescontent()
        {
            AddDiabetesContentViewModel _AddDiabetesContentViewModel = new AddDiabetesContentViewModel();
            _AddDiabetesContentViewModel.PriorityList = new List<PriorityListModel>{
                new PriorityListModel{
                    id = "1", Priority = "Top"
                },
                new PriorityListModel{
                    id = "2", Priority = "Low"
                }
            };

            _AddDiabetesContentViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddDiabetesContentViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            //return View(DiabetesModel);

            //AddDiabetesContentViewModel _AddDiabetesContentViewModel = new AddDiabetesContentViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Diabetes_Content _Diabetes_Content = new Diabetes_Content();
                _Diabetes_Content = _cdhsCommonMethods.GetDataDiabetesContentById(Request.QueryString["edid"].ToString());

                _AddDiabetesContentViewModel.contentid = _Diabetes_Content.Content_Id;
                _AddDiabetesContentViewModel.categoryname = _Diabetes_Content.Category_Name;
                _AddDiabetesContentViewModel.sponsorid = _Diabetes_Content.Sponsar_id;
                _AddDiabetesContentViewModel.Title = _Diabetes_Content.Content_Title;
                _AddDiabetesContentViewModel.DiabetesContaintDetails = _Diabetes_Content.Diabetes_Containt_Details;
                _AddDiabetesContentViewModel.NewsDate = _Diabetes_Content.News_Date;
                _AddDiabetesContentViewModel.Priority = _Diabetes_Content.Priority;
                _AddDiabetesContentViewModel.SelectedApprovalStatus = _Diabetes_Content.Approval_Status;
                _AddDiabetesContentViewModel.SelectedAssetStatus = _Diabetes_Content.Asset_Type;
                _AddDiabetesContentViewModel.UploadPhoto = _Diabetes_Content.Upload_Photo;
                //_AddDiabetesContentViewModel.UploadPhoto = _Diabetes_Content.Upload_Photo;

                _AddDiabetesContentViewModel.id = _Diabetes_Content.id;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddDiabetesContentViewModel);
            }
            return View(_AddDiabetesContentViewModel);
        }
        [HttpPost]
        public ActionResult adddiabetescontent(FormCollection _FormCollect)
        {
                int result = 0;
                AddDiabetesContentViewModel _AddDiabetesContentViewModel = new AddDiabetesContentViewModel();
                _AddDiabetesContentViewModel.PriorityList = new List<PriorityListModel>{
                new PriorityListModel{
                    id = "1", Priority = "Top"
                },
                new PriorityListModel{
                    id = "2", Priority = "Low"
                }
            };

                _AddDiabetesContentViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

                _AddDiabetesContentViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };

            string folderid = string.Empty;
            if (TempData["oldimage"] != null)
            {
                folderid = TempData["oldimage"].ToString();
                if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                {
                    Diabetes_Content dbaddaboutcdhs = new Diabetes_Content();
                    dbaddaboutcdhs.Content_Id = _FormCollect["ContentTypeName"].ToString();
                    dbaddaboutcdhs.Category_Name = _FormCollect["CompanyName"].ToString();
                    dbaddaboutcdhs.Sponsar_id = _FormCollect["sponsorid"].ToString();
                    dbaddaboutcdhs.Content_Title = _FormCollect["Title"].ToString();
                    dbaddaboutcdhs.Diabetes_Containt_Details = _FormCollect["DiabetesContaintDetails"].ToString();
                    dbaddaboutcdhs.News_Date = DateTime.ParseExact(_FormCollect["NewsDate"].ToString(), "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    dbaddaboutcdhs.Priority = _FormCollect["SelectedPriority"].ToString();
                    dbaddaboutcdhs.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    dbaddaboutcdhs.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    dbaddaboutcdhs.Upload_Photo = folderid;
                    //dbaddaboutcdhs.Upload_Photo = HttpUtility.HtmlDecode(_FormCollect["Upload_Photo"].ToString());
                    dbaddaboutcdhs.id = _FormCollect["id"].ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.adddiabetescontentEdit(dbaddaboutcdhs);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Updated Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Update Failed";
                    }
                }
                else
                {
                    Diabetes_Content dbaddaboutcdhs = new Diabetes_Content();
                    dbaddaboutcdhs.Content_Id = _FormCollect["contentid"].ToString();
                    dbaddaboutcdhs.Category_Name = _FormCollect["categoryid"].ToString();
                    dbaddaboutcdhs.Sponsar_id = _FormCollect["CompanyName"].ToString();
                    dbaddaboutcdhs.Content_Title = _FormCollect["Title"] != null ? _FormCollect["Title"].ToString() : "";
                    dbaddaboutcdhs.Diabetes_Containt_Details = _FormCollect["DiabetesContaintDetails"].ToString();
                    dbaddaboutcdhs.News_Date = DateTime.ParseExact(_FormCollect["NewsDate"], "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    dbaddaboutcdhs.Priority = _FormCollect["SelectedPriority"] != null ? _FormCollect["SelectedPriority"].ToString() : "";
                    dbaddaboutcdhs.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    dbaddaboutcdhs.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    dbaddaboutcdhs.Upload_Photo = folderid;
                    //dbaddaboutcdhs.Upload_Photo = HttpUtility.HtmlDecode(_FormCollect["Upload_Photo"].ToString());
                    dbaddaboutcdhs.id = Guid.NewGuid().ToString().Trim();
                    TempData.Clear();
                    result = _cdhsCommonMethods.adddiabetescontentAdd(dbaddaboutcdhs);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Inserted Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Insert Failed";
                    }
                }
            }
            return View ( _AddDiabetesContentViewModel);
        }
        #endregion

        public ActionResult Upload_Photo()
        {
            int result = 0;
            string folderid = string.Empty;
            HttpPostedFileBase file = Request.Files[0];
            string file_name = Request.Files[0].FileName;
            string ext = Path.GetExtension(file_name);
            if (TempData["oldimage"] != null)
            {
                string foldername = TempData["oldimage"].ToString().Substring(0, TempData["oldimage"].ToString().IndexOf('.'));
                DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/images/Diabetes/" + foldername));
                folderid = foldername;
                TempData.Keep("oldimage");
                //if (dir.Exists)
                //{
                //    dir.Delete(true);                    
                //}
            }
            else
            {
                folderid = Guid.NewGuid().ToString();
                TempData["oldimage"] = folderid + ext;
                TempData.Keep("oldimage");
            }
            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    result = _cdhsCommonMethods.UploaddiabetescontImage(file, folderid);
                }
            }
            if (result == 1)
            {
                ViewData["message"] = "File Uploaded Successfully";
            }
            else
            {
                ViewData["message"] = "File Upload Failed";
            }
            return Json(new { Message = ViewData["message"].ToString() });

        }

        #region DiabetesSymptoms_cdhs
        public ActionResult viewdiabetessymptoms()
        {
            IEnumerable<Diabetes_Symptoms> _Diabetes_Symptoms = _cdhsCommonMethods.GetDiabetesSymptomscdhsData();
            ViewDiabetesSymptomsViewModel _ViewDiabetesSymptomsViewModel = new ViewDiabetesSymptomsViewModel();
            // Action list for temprory use delete it later
            _ViewDiabetesSymptomsViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewDiabetesSymptomsViewModel.diabetessymptomscdhs = _Diabetes_Symptoms;

            return View(_ViewDiabetesSymptomsViewModel);
        }
        [HttpPost]
        public ActionResult viewdiabetessymptoms(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("adddiabetessymptoms", "Diabetes", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewdiabetessymptoms", "Diabetes");
        }
        public ActionResult adddiabetessymptoms()
        {
            AddDiabetesSymptomsViewModel _AddDiabetesSymptomsViewModel = new AddDiabetesSymptomsViewModel();
            _AddDiabetesSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddDiabetesSymptomsViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            //return View(DiabetesModel);

            //AddDiabetesContentViewModel _AddDiabetesContentViewModel = new AddDiabetesContentViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Diabetes_Symptoms _Diabetes_Symptoms = new Diabetes_Symptoms();
                _Diabetes_Symptoms = _cdhsCommonMethods.GetDataDiabetesSymptomsById(Request.QueryString["edid"].ToString());

                _AddDiabetesSymptomsViewModel.DiabetesSymptomsTitle = _Diabetes_Symptoms.Diabetes_Symptoms_Title;
                _AddDiabetesSymptomsViewModel.CompanyName = _Diabetes_Symptoms.Sponsor_id;
                _AddDiabetesSymptomsViewModel.ShortDescription = _Diabetes_Symptoms.Short_Description;
                _AddDiabetesSymptomsViewModel.DiabetesSymptomsDetails = _Diabetes_Symptoms.Diabetes_Symptoms_Details;
                _AddDiabetesSymptomsViewModel.SelectedApprovalStatus = _Diabetes_Symptoms.Approval_Status;
                _AddDiabetesSymptomsViewModel.SelectedAssetStatus = _Diabetes_Symptoms.Asset_Type;
                _AddDiabetesSymptomsViewModel.UploadTitleImage = _Diabetes_Symptoms.Upload_Title_Image;
                _AddDiabetesSymptomsViewModel.Priority = _Diabetes_Symptoms.Priority;

                _AddDiabetesSymptomsViewModel.id = _Diabetes_Symptoms.id;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddDiabetesSymptomsViewModel);
            }
            return View(_AddDiabetesSymptomsViewModel);
        }
        [HttpPost]
        public ActionResult adddiabetessymptoms(FormCollection _FormCollect)
        {
            int result = 0;
            AddDiabetesSymptomsViewModel _AddDiabetesSymptomsViewModel = new AddDiabetesSymptomsViewModel();
            _AddDiabetesSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddDiabetesSymptomsViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            string folderid = string.Empty;
            if (TempData["oldimage"] != null)
            {
                folderid = TempData["oldimage"].ToString();
                if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                {
                    Diabetes_Symptoms _Diabetes_Symptoms = new Diabetes_Symptoms();
                    _Diabetes_Symptoms.Diabetes_Symptoms_Title = _FormCollect["DiabetesSymptomsTitle"].ToString();
                    _Diabetes_Symptoms.Sponsor_id = _FormCollect["CompanyName"].ToString();
                    _Diabetes_Symptoms.Short_Description = _FormCollect["ShortDescription"].ToString();
                    _Diabetes_Symptoms.Diabetes_Symptoms_Details = _FormCollect["DiabetesSymptomsDetails"].ToString();
                    _Diabetes_Symptoms.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Diabetes_Symptoms.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    _Diabetes_Symptoms.Upload_Title_Image = folderid;
                    //_Diabetes_Symptoms.Priority = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Diabetes_Symptoms.id = _FormCollect["id"].ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.adddiabetessymptomsEdit(_Diabetes_Symptoms);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Updated Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Update Failed";
                    }
                }
                else
                {
                    Diabetes_Symptoms _Diabetes_Symptoms = new Diabetes_Symptoms();
                    _Diabetes_Symptoms.Diabetes_Symptoms_Title = _FormCollect["DiabetesSymptomsTitle"].ToString();
                    _Diabetes_Symptoms.Sponsor_id = _FormCollect["CompanyName"].ToString();
                    _Diabetes_Symptoms.Short_Description = _FormCollect["ShortDescription"].ToString();
                    _Diabetes_Symptoms.Diabetes_Symptoms_Details = _FormCollect["DiabetesSymptomsDetails"].ToString();
                    _Diabetes_Symptoms.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Diabetes_Symptoms.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    _Diabetes_Symptoms.Upload_Title_Image = folderid;
                    //_Diabetes_Symptoms.Priority = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Diabetes_Symptoms.id = Guid.NewGuid().ToString().Trim();
                    TempData.Clear();
                    result = _cdhsCommonMethods.adddiabetessymptomsAdd(_Diabetes_Symptoms);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Inserted Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Insert Failed";
                    }
                }
        }
            return View(_AddDiabetesSymptomsViewModel);
        }
        #endregion

        public ActionResult Upload_Title_Image()
        {
            int result = 0;
            string folderid = string.Empty;
            HttpPostedFileBase file = Request.Files[0];
            string file_name = Request.Files[0].FileName;
            string ext = Path.GetExtension(file_name);
            if (TempData["oldimage"] != null)
            {
                string foldername = TempData["oldimage"].ToString().Substring(0, TempData["oldimage"].ToString().IndexOf('.'));
                DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/images/Diabetes/" + foldername));
                folderid = foldername;
                TempData.Keep("oldimage");
                //if (dir.Exists)
                //{
                //    dir.Delete(true);                    
                //}
            }
            else
            {
                folderid = Guid.NewGuid().ToString();
                TempData["oldimage"] = folderid + ext;
                TempData.Keep("oldimage");
            }
            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    result = _cdhsCommonMethods.UploaddiabetessymImage(file, folderid);
                }
            }
            if (result == 1)
            {
                ViewData["message"] = "File Uploaded Successfully";
            }
            else
            {
                ViewData["message"] = "File Upload Failed";
            }
            return Json(new { Message = ViewData["message"].ToString() });

        }
	}
}