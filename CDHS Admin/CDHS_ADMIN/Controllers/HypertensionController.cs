﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CDHS_ADMIN.Models;
using System.Globalization;
using System.Web.Hosting;
using System.IO;


namespace CDHS_ADMIN.Controllers
{
    public class HypertensionController : Controller
    {
        private cdhsCommonMethods _cdhsCommonMethods = new cdhsCommonMethods();

        //public ActionResult addhypertensioncontent()
        //{
        //    AddHypertensionContentViewModel _AddHypertensionContentViewModel = new AddHypertensionContentViewModel();
        //    _AddHypertensionContentViewModel.PriorityList = new List<PriorityListModel>{
        //        new PriorityListModel{
        //            id = "1", Priority = "Top"
        //        },
        //        new PriorityListModel{
        //            id = "2", Priority = "Low"
        //        }
        //    };

        //    _AddHypertensionContentViewModel.ApprovalList = new List<ApprovalListModel>{
        //        new ApprovalListModel{
        //            ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
        //        },
        //        new ApprovalListModel{
        //            ApprovalId = "2", ApprovalStatus = "Approved"
        //        },
        //        new ApprovalListModel{
        //            ApprovalId = "3", ApprovalStatus = "Reject"
        //        }
        //    };

        //    _AddHypertensionContentViewModel.AssetList = new List<AssetListModel>{
        //        new AssetListModel{
        //            AssetId = "1", AssetStatus = "Image"
        //        },
        //        new AssetListModel{
        //            AssetId = "2", AssetStatus = "Video"
        //        }
        //    };
        //    return View(_AddHypertensionContentViewModel);
        //}
        //public ActionResult addhypertensionsymptoms()
        //{
        //    AddHypertensionSymptomsViewModel _AddHypertensionSymptomsViewModel = new AddHypertensionSymptomsViewModel();
        //    _AddHypertensionSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
        //        new ApprovalListModel{
        //            ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
        //        },
        //        new ApprovalListModel{
        //            ApprovalId = "2", ApprovalStatus = "Approved"
        //        },
        //        new ApprovalListModel{
        //            ApprovalId = "3", ApprovalStatus = "Reject"
        //        }
        //    };

        //    _AddHypertensionSymptomsViewModel.AssetList = new List<AssetListModel>{
        //        new AssetListModel{
        //            AssetId = "1", AssetStatus = "Image"
        //        },
        //        new AssetListModel{
        //            AssetId = "2", AssetStatus = "Video"
        //        }
        //    };
        //    return View(_AddHypertensionSymptomsViewModel);
        //}
        //public ActionResult viewhypertensioncontent()
        //{
        //    ViewHypertensionSymptomsViewModel _ViewHypertensionSymptomsViewModel = new ViewHypertensionSymptomsViewModel();
        //    _ViewHypertensionSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
        //        new ApprovalListModel{
        //            ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
        //        },
        //        new ApprovalListModel{
        //            ApprovalId = "2", ApprovalStatus = "Approved"
        //        },
        //        new ApprovalListModel{
        //            ApprovalId = "3", ApprovalStatus = "Reject"
        //        }
        //    };

        //    _ViewHypertensionSymptomsViewModel.AssetList = new List<AssetListModel>{
        //        new AssetListModel{
        //            AssetId = "1", AssetStatus = "Action"
        //        },
        //        new AssetListModel{
        //            AssetId = "2", AssetStatus = "View"
        //        },
        //                        new AssetListModel{
        //            AssetId = "3", AssetStatus = "Edit"
        //        },
        //                        new AssetListModel{
        //            AssetId = "4", AssetStatus = "Delete"
        //        }
        //    };
        //    return View(_ViewHypertensionSymptomsViewModel);
        //}

        #region Auto Complete Diabetes Content
        public ActionResult AutocompleteDiabetesContent(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentTypeGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Auto Complete Diabetes Category
        public ActionResult AutocompleteDiabetesCategory(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentCategoryGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Auto Complete Diabetes Company
        public ActionResult AutocompleteDiabetesCompany(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentCompanyGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Auto Complete Hypertension Symptoms Sponsors Name
        public ActionResult AutoCompleteHypertensionSymptoms(string searchstring)
        {
            JsonResult strSearch = _cdhsCommonMethods.ContentSymptomsGetSeacrchString(searchstring);
            return Json(strSearch, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region HypertensionContent_cdhs
        public ActionResult viewhypertensioncontent()
        {
            IEnumerable<Hyper_Tension_Content> _Hyper_Tension_Content = _cdhsCommonMethods.GetHypertensionContentcdhsData();
            ViewHypertensionContentViewModel _ViewHypertensionContentViewModel = new ViewHypertensionContentViewModel();
            // Action list for temprory use delete it later
            _ViewHypertensionContentViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewHypertensionContentViewModel.hypertensioncontentcdhs = _Hyper_Tension_Content;

            return View(_ViewHypertensionContentViewModel);
        }
        [HttpPost]
        public ActionResult viewhypertensioncontent(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addhypertensioncontent", "Hypertension", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewhypertensioncontent", "Hypertension");
        }
        public ActionResult addhypertensioncontent()
        {
            AddHypertensionContentViewModel _AddHypertensionContentViewModel = new AddHypertensionContentViewModel();
            _AddHypertensionContentViewModel.PriorityList = new List<PriorityListModel>{
                new PriorityListModel{
                    id = "1", Priority = "Top"
                },
                new PriorityListModel{
                    id = "2", Priority = "Low"
                }
            };

            _AddHypertensionContentViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddHypertensionContentViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            //return View(DiabetesModel);

            //AddDiabetesContentViewModel _AddDiabetesContentViewModel = new AddDiabetesContentViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Hyper_Tension_Content _Hyper_Tension_Content = new Hyper_Tension_Content();
                _Hyper_Tension_Content = _cdhsCommonMethods.GetDataHypertensionContentById(Request.QueryString["edid"].ToString());

                _AddHypertensionContentViewModel.ContentTypeName = _Hyper_Tension_Content.Content_id;
                _AddHypertensionContentViewModel.categoryname = _Hyper_Tension_Content.Content_Category_Name;
                _AddHypertensionContentViewModel.CompanyName = _Hyper_Tension_Content.Sponsers_id;
                _AddHypertensionContentViewModel.Title = _Hyper_Tension_Content.Title;
                _AddHypertensionContentViewModel.HyperTensionContaintDetails = _Hyper_Tension_Content.Hyper_Tension_Containt_Details;
                _AddHypertensionContentViewModel.NewsDate = _Hyper_Tension_Content.Date;
                _AddHypertensionContentViewModel.SelectedPriority = _Hyper_Tension_Content.Priority;
                _AddHypertensionContentViewModel.SelectedApprovalStatus = _Hyper_Tension_Content.Approval_Status;
                _AddHypertensionContentViewModel.SelectedAssetStatus = _Hyper_Tension_Content.Asset_Type;
                _AddHypertensionContentViewModel.UploadPhoto = _Hyper_Tension_Content.Upload_Photo;

                _AddHypertensionContentViewModel.id = _Hyper_Tension_Content.id;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddHypertensionContentViewModel);
            }
            return View(_AddHypertensionContentViewModel);
        }
        [HttpPost]
        public ActionResult addhypertensioncontent(FormCollection _FormCollect)
        {
            int result = 0;
            AddHypertensionContentViewModel _AddHypertensionContentViewModel = new AddHypertensionContentViewModel();
            _AddHypertensionContentViewModel.PriorityList = new List<PriorityListModel>{
                new PriorityListModel{
                    id = "1", Priority = "Top"
                },
                new PriorityListModel{
                    id = "2", Priority = "Low"
                }
            };

            _AddHypertensionContentViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddHypertensionContentViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            string folderid = string.Empty;
            if (TempData["oldimage"] != null)
            {
                folderid = TempData["oldimage"].ToString();
                if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                {
                    Hyper_Tension_Content _Hyper_Tension_Content = new Hyper_Tension_Content();
                    _Hyper_Tension_Content.Content_id = _FormCollect["ContentTypeName"].ToString();
                    _Hyper_Tension_Content.Content_Category_Name = _FormCollect["categoryname"].ToString();
                    _Hyper_Tension_Content.Sponsers_id = _FormCollect["CompanyName"].ToString();
                    _Hyper_Tension_Content.Title = _FormCollect["Title"].ToString();
                    _Hyper_Tension_Content.Hyper_Tension_Containt_Details = _FormCollect["HyperTensionContaintDetails"].ToString();
                    _Hyper_Tension_Content.Date = DateTime.ParseExact(_FormCollect["NewsDate"].ToString(), "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    _Hyper_Tension_Content.Priority = _FormCollect["SelectedPriority"] != null ? _FormCollect["SelectedPriority"].ToString() : "";
                    _Hyper_Tension_Content.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Hyper_Tension_Content.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    _Hyper_Tension_Content.Upload_Photo = folderid;
                    _Hyper_Tension_Content.id = _FormCollect["id"].ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addhypertensioncontentEdit(_Hyper_Tension_Content);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Updated Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Update Failed";
                    }
                }
                else
                {
                    Hyper_Tension_Content _Hyper_Tension_Content = new Hyper_Tension_Content();
                    _Hyper_Tension_Content.Content_id = _FormCollect["ContentTypeName"].ToString();
                    _Hyper_Tension_Content.Content_Category_Name = _FormCollect["categoryname"].ToString();
                    _Hyper_Tension_Content.Sponsers_id = _FormCollect["CompanyName"].ToString().Trim();
                    _Hyper_Tension_Content.Title = _FormCollect["Title"].ToString();
                    _Hyper_Tension_Content.Hyper_Tension_Containt_Details = _FormCollect["HyperTensionContaintDetails"].ToString();
                    _Hyper_Tension_Content.Date = DateTime.ParseExact(_FormCollect["NewsDate"].ToString(), "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    _Hyper_Tension_Content.Priority = _FormCollect["SelectedPriority"] != null ? _FormCollect["SelectedPriority"].ToString() : "";
                    _Hyper_Tension_Content.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Hyper_Tension_Content.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    _Hyper_Tension_Content.Upload_Photo = folderid;
                    _Hyper_Tension_Content.id = Guid.NewGuid().ToString().Trim();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addhypertensioncontentAdd(_Hyper_Tension_Content);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Inserted Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Insert Failed";
                    }
                }
            }
            return View( _AddHypertensionContentViewModel);
        }
        #endregion

        public ActionResult Upload_Title_Image()
        {
            int result = 0;
            string folderid = string.Empty;
            HttpPostedFileBase file = Request.Files[0];
            string file_name = Request.Files[0].FileName;
            string ext = Path.GetExtension(file_name);
            if (TempData["oldimage"] != null)
            {
                string foldername = TempData["oldimage"].ToString().Substring(0, TempData["oldimage"].ToString().IndexOf('.'));
                DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/images/Hypertension/" + foldername));
                folderid = foldername;
                TempData.Keep("oldimage");
                //if (dir.Exists)
                //{
                //    dir.Delete(true);                    
                //}
            }
            else
            {
                folderid = Guid.NewGuid().ToString();
                TempData["oldimage"] = folderid + ext;
                TempData.Keep("oldimage");
            }
            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    result = _cdhsCommonMethods.UploadHypertensionContImage(file, folderid);
                }
            }
            if (result == 1)
            {
                ViewData["message"] = "File Uploaded Successfully";
            }
            else
            {
                ViewData["message"] = "File Upload Failed";
            }
            return Json(new { Message = ViewData["message"].ToString() });

        }

        #region HypertensionSymptoms_cdhs
        public ActionResult viewhypertensionsymptoms()
        {
            IEnumerable<Hyper_Tension_Symptoms> _Hyper_Tension_Symptoms = _cdhsCommonMethods.GetHypertensionSymptomscdhsData();
            ViewHypertensionSymptomsViewModel _ViewHypertensionSymptomsViewModel = new ViewHypertensionSymptomsViewModel();
            // Action list for temprory use delete it later
            _ViewHypertensionSymptomsViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewHypertensionSymptomsViewModel.hypertensionsymptomscdhs = _Hyper_Tension_Symptoms;

            return View(_ViewHypertensionSymptomsViewModel);
        }
        [HttpPost]
        public ActionResult viewhypertensionsymptoms(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addhypertensionsymptoms", "Hypertension", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewhypertensionsymptoms", "Hypertension");
        }
        public ActionResult addhypertensionsymptoms()
        {
            AddHypertensionSymptomsViewModel _AddHypertensionSymptomsViewModel = new AddHypertensionSymptomsViewModel();
            _AddHypertensionSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddHypertensionSymptomsViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            //return View(DiabetesModel);

            //AddDiabetesContentViewModel _AddDiabetesContentViewModel = new AddDiabetesContentViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Hyper_Tension_Symptoms _Hyper_Tension_Symptoms = new Hyper_Tension_Symptoms();
                _Hyper_Tension_Symptoms = _cdhsCommonMethods.GetDataHypertensionSymptomsById(Request.QueryString["edid"].ToString());

                _AddHypertensionSymptomsViewModel.HyperTensionSymptomsTitle = _Hyper_Tension_Symptoms.Hyper_Tension_Symptoms_Title;
                _AddHypertensionSymptomsViewModel.CompanyName = _Hyper_Tension_Symptoms.Sponser_id;
                _AddHypertensionSymptomsViewModel.ShortDescription = _Hyper_Tension_Symptoms.Short_Description;
                _AddHypertensionSymptomsViewModel.HyperTensionSymptomsDetails = _Hyper_Tension_Symptoms.Hyper_Tension_Symptoms_Details;
                _AddHypertensionSymptomsViewModel.SelectedApprovalStatus = _Hyper_Tension_Symptoms.Approval_Status;
                _AddHypertensionSymptomsViewModel.SelectedAssetStatus = _Hyper_Tension_Symptoms.Asset_Type;
                _AddHypertensionSymptomsViewModel.UploadTitleImage = _Hyper_Tension_Symptoms.Upload_Title_Image;
                _AddHypertensionSymptomsViewModel.Priority = _Hyper_Tension_Symptoms.Priority;

                _AddHypertensionSymptomsViewModel.id = _Hyper_Tension_Symptoms.id;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddHypertensionSymptomsViewModel);
            }
            return View(_AddHypertensionSymptomsViewModel);
        }
        [HttpPost]
        public ActionResult addhypertensionsymptoms(FormCollection _FormCollect)
        {
            int result = 0;
            AddHypertensionSymptomsViewModel _AddHypertensionSymptomsViewModel = new AddHypertensionSymptomsViewModel();
            _AddHypertensionSymptomsViewModel.ApprovalList = new List<ApprovalListModel>{
                new ApprovalListModel{
                    ApprovalId = "1", ApprovalStatus = "Awaiting For Approval"
                },
                new ApprovalListModel{
                    ApprovalId = "2", ApprovalStatus = "Approved"
                },
                new ApprovalListModel{
                    ApprovalId = "3", ApprovalStatus = "Reject"
                }
            };

            _AddHypertensionSymptomsViewModel.AssetList = new List<AssetListModel>{
                new AssetListModel{
                    AssetId = "1", AssetStatus = "Image"
                },
                new AssetListModel{
                    AssetId = "2", AssetStatus = "Video"
                }
            };
            string folderid = string.Empty;
            if (TempData["oldimage"] != null)
            {
                folderid = TempData["oldimage"].ToString();
                if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                {
                    Hyper_Tension_Symptoms _Hyper_Tension_Symptoms = new Hyper_Tension_Symptoms();
                    _Hyper_Tension_Symptoms.Hyper_Tension_Symptoms_Title = _FormCollect["HyperTensionSymptomsTitle"].ToString();
                    _Hyper_Tension_Symptoms.Sponser_id = _FormCollect["CompanyName"].ToString();
                    _Hyper_Tension_Symptoms.Short_Description = _FormCollect["ShortDescription"].ToString();
                    _Hyper_Tension_Symptoms.Hyper_Tension_Symptoms_Details = _FormCollect["HyperTensionSymptomsDetails"].ToString();
                    _Hyper_Tension_Symptoms.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Hyper_Tension_Symptoms.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    _Hyper_Tension_Symptoms.Upload_Title_Image = folderid;
                    //_Diabetes_Symptoms.Priority = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Hyper_Tension_Symptoms.id = _FormCollect["id"].ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addhypertensionsymptomsEdit(_Hyper_Tension_Symptoms);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Updated Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Update Failed";
                    }
                }
                else
                {
                    Hyper_Tension_Symptoms _Hyper_Tension_Symptoms = new Hyper_Tension_Symptoms();
                    _Hyper_Tension_Symptoms.Hyper_Tension_Symptoms_Title = _FormCollect["HyperTensionSymptomsTitle"].ToString();
                    _Hyper_Tension_Symptoms.Sponser_id = _FormCollect["CompanyName"].ToString();
                    _Hyper_Tension_Symptoms.Short_Description = _FormCollect["ShortDescription"].ToString();
                    _Hyper_Tension_Symptoms.Hyper_Tension_Symptoms_Details = _FormCollect["HyperTensionSymptomsDetails"].ToString();
                    _Hyper_Tension_Symptoms.Approval_Status = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Hyper_Tension_Symptoms.Asset_Type = _FormCollect["SelectedAssetStatus"].ToString();
                    _Hyper_Tension_Symptoms.Upload_Title_Image = folderid;
                    //_Diabetes_Symptoms.Priority = _FormCollect["SelectedApprovalStatus"].ToString();
                    _Hyper_Tension_Symptoms.id = Guid.NewGuid().ToString().Trim();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addhypertensionsymptomsAdd(_Hyper_Tension_Symptoms);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Inserted Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Insert Failed";
                    }
                }
            }
            return View(_AddHypertensionSymptomsViewModel);
        }
        #endregion

        public ActionResult Upload_Photo()
        {
            int result = 0;
            string folderid = string.Empty;
            HttpPostedFileBase file = Request.Files[0];
            string file_name = Request.Files[0].FileName;
            string ext = Path.GetExtension(file_name);
            if (TempData["oldimage"] != null)
            {
                string foldername = TempData["oldimage"].ToString().Substring(0, TempData["oldimage"].ToString().IndexOf('.'));
                DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/images/Hypertension/" + foldername));
                folderid = foldername;
                TempData.Keep("oldimage");
                //if (dir.Exists)
                //{
                //    dir.Delete(true);                    
                //}
            }
            else
            {
                folderid = Guid.NewGuid().ToString();
                TempData["oldimage"] = folderid + ext;
                TempData.Keep("oldimage");
            }
            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    result = _cdhsCommonMethods.UploadHypertensionSymImage(file, folderid);
                }
            }
            if (result == 1)
            {
                ViewData["message"] = "File Uploaded Successfully";
            }
            else
            {
                ViewData["message"] = "File Upload Failed";
            }
            return Json(new { Message = ViewData["message"].ToString() });

        }
	}
}