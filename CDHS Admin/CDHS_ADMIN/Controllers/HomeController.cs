﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CDHS_ADMIN.Models;
using System.Web.Hosting;
using System.IO;

namespace CDHS_ADMIN.Controllers
{
    public class HomeController : Controller
    {
        private cdhsCommonMethods _cdhsCommonMethods = new cdhsCommonMethods();

        #region About_cdhs
        //[AuthorizeUser]

        public ActionResult aboutcdhs()
        {
            IEnumerable<About_cdhs> _About_cdhs = _cdhsCommonMethods.GetData();

            return View(_About_cdhs);
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult viewaboutcdhs()
        {
            IEnumerable<About_cdhs> _About_cdhs = _cdhsCommonMethods.GetAboutcdhsData();
            ViewAboutcdhsViewModel _ViewAboutcdhsViewModel = new ViewAboutcdhsViewModel();
            // Action list for temprory use delete it later
            _ViewAboutcdhsViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewAboutcdhsViewModel.aboutcdhs = _About_cdhs;

            return View(_ViewAboutcdhsViewModel);
        }
        [HttpPost]
        public ActionResult viewaboutcdhs(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addaboutcdhs", "Home", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewaboutcdhs", "Home");
        }
        public ActionResult addaboutcdhs()
        {
            AddAboutCdhsViewModel _AddAboutCdhsViewModel = new AddAboutCdhsViewModel();
            if (Request.QueryString["edid"] != null)
            {
                About_cdhs _About_cdhs = new About_cdhs();
                _About_cdhs = _cdhsCommonMethods.GetDataById(Request.QueryString["edid"].ToString());

                _AddAboutCdhsViewModel.Title = _About_cdhs.Title;
                _AddAboutCdhsViewModel.ContentinDetail = _About_cdhs.Content_in_Detail;
                _AddAboutCdhsViewModel.id = _About_cdhs.id;
                _AddAboutCdhsViewModel.UploadTitleImage = _About_cdhs.Upload_Title_Image;
                _AddAboutCdhsViewModel.Priority = _About_cdhs.Priority;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddAboutCdhsViewModel);
            }
            return View(_AddAboutCdhsViewModel);
        }
        [HttpPost]
        public ActionResult addaboutcdhs(FormCollection _FormCollect)
        {
                int result = 0;
                string folderid = string.Empty;
                if (TempData["oldimage"] != null)
                {
                    folderid = TempData["oldimage"].ToString();
                    if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                    {
                        About_cdhs dbaddaboutcdhs = new About_cdhs();
                        dbaddaboutcdhs.Title = _FormCollect["Title"].ToString();
                        dbaddaboutcdhs.Content_in_Detail = HttpUtility.HtmlDecode(_FormCollect["ContentinDetail"].ToString());
                        dbaddaboutcdhs.Upload_Title_Image = folderid;
                        dbaddaboutcdhs.id = _FormCollect["id"].ToString();
                        TempData.Clear();
                        result = _cdhsCommonMethods.addaboutcdhsEdit(dbaddaboutcdhs);
                        if (result == 1)
                        {
                            ViewData["message"] = "Record Updated Successfully";
                        }
                        else
                        {
                            ViewData["message"] = "Record Update Failed";
                        }
                    }
                    else
                    {
                        About_cdhs dbaddaboutcdhs = new About_cdhs();
                        dbaddaboutcdhs.Title = _FormCollect["Title"].ToString();
                        dbaddaboutcdhs.Content_in_Detail = HttpUtility.HtmlDecode(_FormCollect["ContentinDetail"].ToString());
                        dbaddaboutcdhs.Upload_Title_Image = folderid;
                        dbaddaboutcdhs.id = Guid.NewGuid().ToString().Trim();
                        TempData.Clear();
                        result = _cdhsCommonMethods.addaboutcdhsAdd(dbaddaboutcdhs);
                        if (result == 1)
                        {
                            ViewData["message"] = "Record Inserted Successfully";
                        }
                        else
                        {
                            ViewData["message"] = "Record Insert Failed";
                        }
                    }
                }
                    return View(new AddAboutCdhsViewModel());
            }
 
         #endregion

        public ActionResult Upload_Title_Image()
        {
            int result = 0;
            string folderid = string.Empty;
            HttpPostedFileBase file = Request.Files[0];
            string file_name = Request.Files[0].FileName;
            string ext = Path.GetExtension(file_name);
            if (TempData["oldimage"] != null)
            {
                string foldername = TempData["oldimage"].ToString().Substring(0, TempData["oldimage"].ToString().IndexOf('.'));
                DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/images/Home/" + foldername));
                folderid = foldername;
                TempData.Keep("oldimage");
                //if (dir.Exists)
                //{
                //    dir.Delete(true);                    
                //}
            }
            else
            {
                folderid = Guid.NewGuid().ToString();
                TempData["oldimage"] = folderid + ext;
                TempData.Keep("oldimage");
            }
            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    result = _cdhsCommonMethods.UploadStoreImage(file, folderid);
                }
            }
            if (result == 1)
            {
                ViewData["message"] = "File Uploaded Successfully";
            }
            else
            {
                ViewData["message"] = "File Upload Failed";
            }
            return Json(new { Message = ViewData["message"].ToString() });

        }

        #region Content_Category
        public ActionResult addcategory()
        {
            AddCategoryViewModel _AddCategoryViewModel = new AddCategoryViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Content_Category _Content_Category = new Content_Category();
                _Content_Category = _cdhsCommonMethods.GetDatacategoryById(Request.QueryString["edid"].ToString());

                _AddCategoryViewModel.CategoryName = _Content_Category.Category_Name;
                _AddCategoryViewModel.CategoryDescription = _Content_Category.Category_Description;
                _AddCategoryViewModel.id = _Content_Category.id;
                _AddCategoryViewModel.Priority = _Content_Category.Priority;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddCategoryViewModel);
            }
            return View(_AddCategoryViewModel);
        }

        [HttpPost]
        public ActionResult addcategory(FormCollection _FormCollect)
        {
            int result = 0;
                if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                {
                    Content_Category dbcontcategory = new Content_Category();
                    dbcontcategory.Category_Name = _FormCollect["CategoryName"].ToString();
                    dbcontcategory.Category_Description = HttpUtility.HtmlDecode(_FormCollect["CategoryDescription"].ToString());
                    dbcontcategory.id = _FormCollect["id"].ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addcategoryEdit(dbcontcategory);
                }
                else
                {
                    Content_Category dbaddcategory = new Content_Category();
                    dbaddcategory.Category_Name = _FormCollect["CategoryName"] != null ? _FormCollect["CategoryName"].ToString() : "";
                    dbaddcategory.Category_Description = HttpUtility.HtmlDecode(_FormCollect["CategoryDescription"] != null ? _FormCollect["CategoryDescription"].ToString() : "");
                    dbaddcategory.id = Guid.NewGuid().ToString().Trim();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addcategoryAdd(dbaddcategory);
                }
                return View(new AddCategoryViewModel());
        }


        public ActionResult viewcategory()
        {
            IEnumerable<Content_Category> _Content_Category = _cdhsCommonMethods.GetCategorycdhsData();
            ViewCategoryViewModel _ViewCategoryViewModel = new ViewCategoryViewModel();
            // Action list for temprory use delete it later
            _ViewCategoryViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            _ViewCategoryViewModel.categorycdhs = _Content_Category;
            return View(_ViewCategoryViewModel);
        }
        [HttpPost]
        public ActionResult viewcategory(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addcategory", "Home", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewcategory", "Home");
        }
        #endregion

        #region Content_Type
        public ActionResult addcontenttype()
        {
            AddContentTypeViewModel _AddContentTypeViewModel = new AddContentTypeViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Content_Types _Content_Types = new Content_Types();
                _Content_Types = _cdhsCommonMethods.GetDatacontentById(Request.QueryString["edid"].ToString());

                _AddContentTypeViewModel.TypeName = _Content_Types.Type_Name;
                _AddContentTypeViewModel.TypeDescription = _Content_Types.Type_Description;
                _AddContentTypeViewModel.id = _Content_Types.id;
                _AddContentTypeViewModel.Priority = _Content_Types.Priority;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddContentTypeViewModel);
            }
            return View(_AddContentTypeViewModel);
        }
        [HttpPost]
        public ActionResult addcontenttype(FormCollection _FormCollect)
        {
            int result = 0;
            if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                {
                    Content_Types dbaddcontenttype = new Content_Types();
                    dbaddcontenttype.Type_Name = _FormCollect["TypeName"] != null ? _FormCollect["TypeName"].ToString() : "";
                    dbaddcontenttype.Type_Description = HttpUtility.HtmlDecode(_FormCollect["TypeDescription"] != null ? _FormCollect["TypeDescription"].ToString() : "");
                    dbaddcontenttype.id = _FormCollect["id"].ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addcontenttypeEdit(dbaddcontenttype);
                }
                else
                {
                    Content_Types dbaddcontenttype = new Content_Types();
                    dbaddcontenttype.Type_Name = _FormCollect["TypeName"] != null ? _FormCollect["TypeName"].ToString() : "";
                    dbaddcontenttype.Type_Description = HttpUtility.HtmlDecode(_FormCollect["TypeDescription"] != null ? _FormCollect["TypeDescription"].ToString() : "");
                    dbaddcontenttype.id = Guid.NewGuid().ToString().Trim();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addcontenttypeAdd(dbaddcontenttype);
                }
            return View(new AddContentTypeViewModel());
        }

        public ActionResult viewcontenttype()
        {
            IEnumerable<Content_Types> _Content_Types = _cdhsCommonMethods.GetContentcdhsData();
            ViewContentTypeViewModel _ViewContentTypeViewModel = new ViewContentTypeViewModel();
            _ViewContentTypeViewModel.UpcomingList = new List<UpcomingListModel>{
                new UpcomingListModel{
                    UpcomingId = "1", UpcomingStatus = "Action"
                },
                new UpcomingListModel{
                    UpcomingId = "2", UpcomingStatus = "View"
                },
                new UpcomingListModel{
                    UpcomingId = "3", UpcomingStatus = "Edit"
                },
                new UpcomingListModel{
                    UpcomingId = "4", UpcomingStatus = "Delete"
                }
            };
            _ViewContentTypeViewModel.contentcdhs = _Content_Types;
            return View(_ViewContentTypeViewModel);
        }

        [HttpPost]
        public ActionResult viewcontenttype(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addcontenttype", "Home", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewcontenttype", "Home");
        }
        #endregion

        #region Sponsors
        public ActionResult addsponsors()
        {
            AddSponsorsViewModel _AddSponsorsViewModel = new AddSponsorsViewModel();
            if (Request.QueryString["edid"] != null)
            {
                Sponsor _Sponsors = new Sponsor();
                _Sponsors = _cdhsCommonMethods.GetDataSponsorsById(Request.QueryString["edid"].ToString());

                _AddSponsorsViewModel.CompanyName = _Sponsors.Company_Name;
                _AddSponsorsViewModel.Address = _Sponsors.Address;
                _AddSponsorsViewModel.CompanyDescription = _Sponsors.Company_Description;
                _AddSponsorsViewModel.ContactNumber = _Sponsors.Contact_Number;
                _AddSponsorsViewModel.EmailId = _Sponsors.Email_Id;
                _AddSponsorsViewModel.WebsiteURL = _Sponsors.Website_URL;
                _AddSponsorsViewModel.UploadLogo = _Sponsors.Upload_Logo;
                _AddSponsorsViewModel.id = _Sponsors.id;
                _AddSponsorsViewModel.Priority = _Sponsors.Priority;
                //if (_AboutFtw.title_img != null)
                //{
                //    string imagefoldername = _AboutFtwViewModel.image.Substring(0, _AboutFtwViewModel.image.IndexOf('.'));
                //    DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + imagefoldername));
                //    if (dir.Exists)
                //    {
                //        FileInfo[] allfiles = dir.GetFiles();
                //        _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + imagefoldername + "/" + allfiles[0].Name);
                //        // _AboutFtwViewModel.ImagePath = "http://" + Request.Url.Authority + ("/Images/AboutFtw/" + FolderName + "/" + allfiles[0].Name);
                //    }
                //}
                ////  string FolderName = _AboutFtw.title_img;
                //// FolderName = FolderName.Substring(0, FolderName.LastIndexOf('.'));                
                //// DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/Images/AboutFtw/" + FolderName));

                //TempData["oldimage"] = _AboutFtw.title_img;
                //TempData.Keep("oldimage");
                return View(_AddSponsorsViewModel);
            }
            return View(_AddSponsorsViewModel);
        }
        [HttpPost]
        public ActionResult addsponsors(FormCollection _FormCollect)
        {
            int result = 0;
            string folderid = string.Empty;
            if (TempData["oldimage"] != null)
            {
                folderid = TempData["oldimage"].ToString();
                if (_FormCollect["id"] != null && _FormCollect["id"].ToString() != "")
                {
                    Sponsor dbaddsponsors = new Sponsor();
                    dbaddsponsors.Company_Name = _FormCollect["CompanyName"] != null ? _FormCollect["CompanyName"].ToString() : "";
                    dbaddsponsors.Address = _FormCollect["Address"] != null ? _FormCollect["Address"].ToString() : "";
                    dbaddsponsors.Company_Description = _FormCollect["CompanyDescription"] != null ? _FormCollect["CompanyDescription"].ToString() : "";
                    dbaddsponsors.Contact_Number = _FormCollect["ContactNumber"] != null ? _FormCollect["ContactNumber"].ToString() : "";
                    dbaddsponsors.Email_Id = _FormCollect["EmailId"] != null ? _FormCollect["EmailId"].ToString() : "";
                    dbaddsponsors.Website_URL = HttpUtility.HtmlDecode(_FormCollect["WebsiteURL"] != null ? _FormCollect["WebsiteURL"].ToString() : "");
                    dbaddsponsors.Upload_Logo = folderid;
                    dbaddsponsors.id = _FormCollect["id"].ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addsponsorsEdit(dbaddsponsors);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Updated Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Update Failed";
                    }
                }
                else
                {
                    Sponsor dbaddsponsors = new Sponsor();
                    dbaddsponsors.Company_Name = _FormCollect["CompanyName"] != null ? _FormCollect["CompanyName"].ToString() : "";
                    dbaddsponsors.Address = _FormCollect["Address"] != null ? _FormCollect["Address"].ToString() : "";
                    dbaddsponsors.Company_Description = _FormCollect["CompanyDescription"] != null ? _FormCollect["CompanyDescription"].ToString() : "";
                    dbaddsponsors.Contact_Number = _FormCollect["ContactNumber"] != null ? _FormCollect["ContactNumber"].ToString() : "";
                    dbaddsponsors.Email_Id = _FormCollect["EmailId"] != null ? _FormCollect["EmailId"].ToString() : "";
                    dbaddsponsors.Website_URL = HttpUtility.HtmlDecode(_FormCollect["WebsiteURL"] != null ? _FormCollect["WebsiteURL"].ToString() : "");
                    dbaddsponsors.Upload_Logo = folderid;
                    dbaddsponsors.id = Guid.NewGuid().ToString();
                    TempData.Clear();
                    result = _cdhsCommonMethods.addsponsorsAdd(dbaddsponsors);
                    if (result == 1)
                    {
                        ViewData["message"] = "Record Inserted Successfully";
                    }
                    else
                    {
                        ViewData["message"] = "Record Insert Failed";
                    }
                }
            }
            return View(new AddSponsorsViewModel());
        }
        public ActionResult viewsponsors()
        {
            IEnumerable<Sponsor> _Sponsors = _cdhsCommonMethods.GetSponsorscdhsData();
            ViewSponsorsViewModel _ViewSponsorsViewModel = new ViewSponsorsViewModel();
            _ViewSponsorsViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };
            _ViewSponsorsViewModel.sponsorscdhs = _Sponsors;
            return View(_ViewSponsorsViewModel);
        }
        [HttpPost]
        public ActionResult viewsponsors(FormCollection form_collection)
        {
            int result = 0;
            if (form_collection["basicOps"] != null && !string.IsNullOrEmpty(form_collection["basicOps"].Replace(",", "")))
            {
                string[] opType = form_collection["basicOps"].ToString().Split('~');
                if (opType[1].IndexOf("Edit") > -1)
                {
                    return RedirectToAction("addsponsors", "Home", new { id = opType[0].ToString().Trim().Replace(",", ""), edid = opType[0].ToString().Trim().Replace(",", "") });
                }
                else if (opType[1].IndexOf("Delete") > -1)
                {
                    //result = _ftwCommonMethods.AboutFtwDelete(opType[0].ToString().Replace(",", ""));
                    //if (result == 1)
                    //{
                    //    ViewData["message"] = "Record Deleted Successfully";
                    //}
                    //else
                    //{
                    //    ViewData["message"] = "Record Delete Failed";
                    //}
                }
            }
            return RedirectToAction("viewsponsors", "Home");
        }

        public ActionResult Upload_Logo()
        {
            int result = 0;
            string folderid = string.Empty;
            HttpPostedFileBase file = Request.Files[0];
            string file_name = Request.Files[0].FileName;
            string ext = Path.GetExtension(file_name);
            if (TempData["oldimage"] != null)
            {
                string foldername = TempData["oldimage"].ToString().Substring(0, TempData["oldimage"].ToString().IndexOf('.'));
                DirectoryInfo dir = new DirectoryInfo(HostingEnvironment.MapPath("~/images/Sponsor/" + foldername));
                folderid = foldername;
                TempData.Keep("oldimage");
                //if (dir.Exists)
                //{
                //    dir.Delete(true);                    
                //}
            }
            else
            {
                folderid = Guid.NewGuid().ToString();
                TempData["oldimage"] = folderid + ext;
                TempData.Keep("oldimage");
            }
            if (file != null)
            {
                if (file.ContentLength != 0)
                {
                    result = _cdhsCommonMethods.UploadStoreLogo(file, folderid);
                }
            }
            if (result == 1)
            {
                ViewData["message"] = "File Uploaded Successfully";
            }
            else
            {
                ViewData["message"] = "File Upload Failed";
            }
            return Json(new { Message = ViewData["message"].ToString() });

        }
        #endregion

        public ActionResult addusers()
        {
            return View();
        }
        
        public ActionResult index()
        {
            IndexViewModel _IndexViewModel = new IndexViewModel();
            _IndexViewModel.StateList = new List<StateListModel>();

            _IndexViewModel.CityList = new List<CityListModel>();

            _IndexViewModel.AgeList = new List<AgeListModel>();
            return View(_IndexViewModel);
            
        }
        public ActionResult login()
        {
            return View();
        }
        public ActionResult userrights()
        {
            UserRightsViewModel _UserRightsViewModel = new UserRightsViewModel();
            _UserRightsViewModel.LinkList = new List<LinkListModel>{
                new LinkListModel{
                    LinkId = "1", LinkStatus = "Select Menu"
                },
                new LinkListModel{
                    LinkId = "2", LinkStatus = "CDHS"
                },
                new LinkListModel{
                    LinkId = "3", LinkStatus = "Health Tips"
                },
                new LinkListModel{
                    LinkId = "4", LinkStatus = "Symptoms"
                },
                new LinkListModel{
                    LinkId = "5", LinkStatus = "Sponsors"
                },
                new LinkListModel{
                    LinkId = "6", LinkStatus = "Users"
                },
                new LinkListModel{
                    LinkId = "7", LinkStatus = "Health Tracker"
                },
                new LinkListModel{
                    LinkId = "8", LinkStatus = "Sales"
                }
            };
            return View(_UserRightsViewModel);
        }
        
        public ActionResult viewusers()
        {
            ViewUsersViewModel _ViewUsersViewModel = new ViewUsersViewModel();
            _ViewUsersViewModel.ActionList = new List<ActionListModel>{
                new ActionListModel{
                    ActionId = "1", ActionStatus = "Action"
                },
                new ActionListModel{
                    ActionId = "2", ActionStatus = "View"
                },
                new ActionListModel{
                    ActionId = "3", ActionStatus = "Edit"
                },
                new ActionListModel{
                    ActionId = "4", ActionStatus = "Delete"
                }
            };

            return View(_ViewUsersViewModel);
        }
              
    }
}