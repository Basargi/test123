﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddSponsors2ViewModel
    {
        public string CompanyName { get; set; }

        public string Address { get; set; }

        public string CompanyDescription { get; set; }

        public string ContactNumber { get; set; }

        public string EmailId { get; set; }

        public string WebsiteURL { get; set; }


    }
}