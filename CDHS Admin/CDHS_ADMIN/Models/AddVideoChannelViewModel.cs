﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddVideoChannelViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Approval")]
        public IEnumerable<ApprovalListModel> ApprovalList { get; set; }
        public string Approval { get; set; }

        public string Title { get; set; }

        public string Date { get; set; }

        public string SponsorName { get; set; }

        public string ShortDescription { get; set; }

        public string VideoLink { get; set; }

        public string SelectedApprovalStatus { get; set; }


    }
}