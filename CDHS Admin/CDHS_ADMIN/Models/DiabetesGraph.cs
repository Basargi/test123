﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class DiabetesGraph
    {
        public string DiabetesSymptomsTitle { get; set; }

        public string ContentBy { get; set; }

        public string ShortDescription { get; set; }

        public string DiabetesSymptomsDetails { get; set; }

        public string ApprovalStatus { get; set; }

        public string AssetType { get; set; }

        public string UploadTitleImage { get; set; }
    }
}