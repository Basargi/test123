﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewCategoryViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Action")]
        public IEnumerable<ActionListModel> ActionList { get; set; }
        public string Action { get; set; }

        public string SearchByContentCategory { get; set; }

        public bool Loremipsumdolor { get; set; }

        public string SelectedActionStatus { get; set; }

        public IEnumerable<Content_Category> categorycdhs { get; set; }



    }
}