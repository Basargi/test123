﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewSponsorsViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Action")]
        public IEnumerable<ActionListModel> ActionList { get; set; }
        public string Action { get; set; }

        public string SearchBySponsor { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string SelectedActionStatus { get; set; }

        public IEnumerable<Sponsor> sponsorscdhs { get; set; }



    }
}