﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class SalesRepReportViewModel
    {
        public string SearchBySubrub { get; set; }

        public string SearchByState { get; set; }

        public string SearchBySalesRep { get; set; }


    }
}