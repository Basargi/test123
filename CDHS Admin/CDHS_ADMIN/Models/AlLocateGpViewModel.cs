﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;


namespace CDHS_ADMIN.Models
{
    public class AlLocateGpViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Search")]
        public IEnumerable<SearchListModel> SearchList { get; set; }
        public string Search { get; set; }

        public string SearchByState { get; set; }

        public string SearchBySubrub { get; set; }

        public bool Select { get; set; }

        public string SelectedSearchStatus { get; set; }

        public bool active { get; set; }

        public IEnumerable<GP> allocatecdhs { get; set; }

    }
}