﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace CDHS_ADMIN.Models
{
    public class ViewDiabetesContentViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Action")]
        public IEnumerable<ActionListModel> ActionList { get; set; }
        public string Action { get; set; }

        public string SelectedActionStatus { get; set; }

        public string ViewDiabetesContent { get; set; }

        public bool Loremipsumdolor { get; set; }

        public IEnumerable<Diabetes_Content> diabetescontentcdhs { get; set; }


    }
}