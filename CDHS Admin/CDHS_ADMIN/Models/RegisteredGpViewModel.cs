﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class RegisteredGpViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "State")]
        public IEnumerable<StateListModel> StateList { get; set; }
        public string State { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "City")]
        public IEnumerable<CityListModel> CityList { get; set; }
        public string City { get; set; }

        public string SearchByClinic { get; set; }

        public string SelectedStateStatus { get; set; }

        public string SelectedCityStatus { get; set; }


    }
}