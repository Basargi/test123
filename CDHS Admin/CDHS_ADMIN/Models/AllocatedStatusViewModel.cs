﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AllocatedStatusViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Search")]
        public IEnumerable<SearchListModel> SearchList { get; set; }
        public string Search { get; set; }

        public string SelectedSearch { get; set; }

    }
}