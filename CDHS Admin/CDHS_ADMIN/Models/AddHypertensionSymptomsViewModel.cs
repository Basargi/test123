﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddHypertensionSymptomsViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Approval")]
        public IEnumerable<ApprovalListModel> ApprovalList { get; set; }
        public string Approval { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Asset")]
        public IEnumerable<AssetListModel> AssetList { get; set; }
        public string Asset { get; set; }

        public string HyperTensionSymptomsTitle { get; set; }

        public string ContentBy { get; set; }
        public string id { get; set; }
        public string companyname { get; set; }
        public string Priority { get; set; }

        public string CompanyName { get; set; }
        public string sponsorid { get; set; }

        public string ShortDescription { get; set; }

        public string HyperTensionSymptomsDetails { get; set; }

        public string SelectedApprovalStatus { get; set; }

        public string SelectedAssetStatus { get; set; }

        public string UploadTitleImage { get; set; }
    }
}