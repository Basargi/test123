﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewHypertensionContentViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Action")]
        public IEnumerable<ActionListModel> ActionList { get; set; }
        public string Action { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Approval")]
        public IEnumerable<ApprovalListModel> ApprovalList { get; set; }
        public string Approval { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Asset")]
        public IEnumerable<AssetListModel> AssetList { get; set; }
        public string Asset { get; set; }

        public string SelectedActionStatus { get; set; }

        public string SearchByContentType { get; set; }

        public string SearchByContentCategory { get; set; }

        public string SearchByContentSponsors { get; set; }

        public bool Loremipsumdolor { get; set; }

        public bool SelectedApprovalStatus { get; set; }

        public string SelectedAssetStatus { get; set; }

        public IEnumerable<Hyper_Tension_Content> hypertensioncontentcdhs { get; set; }

    }
}