﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddUsersViewModel
    {
        public string Name { get; set; }

        public bool LastName { get; set; }

        public string EmailID { get; set; }

        public string EnterPassword { get; set; }

        public string ConfirmPassword { get; set; }

    }
}