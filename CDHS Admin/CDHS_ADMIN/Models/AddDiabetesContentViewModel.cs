﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddDiabetesContentViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Priority")]
        public IEnumerable<PriorityListModel> PriorityList { get; set; }
        public string Priority { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Approval")]
        public IEnumerable<ApprovalListModel> ApprovalList { get; set; }
        public string Approval { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Asset")]
        public IEnumerable<AssetListModel> AssetList { get; set; }
        public string Asset { get; set; }

        //[DataType(DataType.Text)]
        //[Display(Name = "ContentTypeName")]
        public string ContentTypeName { get; set; }
        public string id { get; set; }
        public string contentid { get; set; }
        public string categoryname { get; set; }
        public string categoryid { get; set; }

        public string sponsorid { get; set; }


        public string ContentCategoryName { get; set; }

        public string CompanyName { get; set; }

        public string Title { get; set; }

        public string DiabetesContaintDetails { get; set; }

        public DateTime NewsDate { get; set; }

        public string SelectedPriority { get; set; }

        public string SelectedApprovalStatus { get; set; }

        public string SelectedAssetStatus { get; set; }

        public string AssetType { get; set; }

        public string UploadPhoto { get; set; }

        public string DragHeretoUpload { get; set; }


    }
 }