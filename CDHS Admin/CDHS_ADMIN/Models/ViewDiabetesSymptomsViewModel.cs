﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class ViewDiabetesSymptomsViewModel
    {
        [DataType(DataType.Text)]
        [Display(Name = "Action")]
        public IEnumerable<ActionListModel> ActionList { get; set; }
        public string Action { get; set; }

        public string SelectedActionStatus { get; set; }
        public string ViewDiabetesSymptoms { get; set; }

        public bool Loremipsumdolor { get; set; }

        public string ShortDescription { get; set; }

        public string DiabetesSymptomsDetails { get; set; }

        public string ApprovalStatus { get; set; }

        public string AssetType { get; set; }

        public string UploadTitleImage { get; set; }

        public IEnumerable<Diabetes_Symptoms> diabetessymptomscdhs { get; set; }

    }
}