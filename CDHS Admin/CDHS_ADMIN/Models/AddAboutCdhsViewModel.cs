﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace CDHS_ADMIN.Models
{
    public class AddAboutCdhsViewModel
    {
        public string Title { get; set; }

        public string id { get; set; }

        public string ContentinDetail { get; set; }

        public string Priority { get; set; }

        public string UploadTitleImage { get; set; }
    }
}