//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CDHS_ADMIN.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Content_Category
    {
        public string id { get; set; }
        public string Category_Name { get; set; }
        public string Category_Description { get; set; }
        public string Priority { get; set; }
    }
}
