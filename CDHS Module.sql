USE [master]
GO
/****** Object:  Database [CDHS]    Script Date: 09-Jan-18 2:11:12 PM ******/
CREATE DATABASE [CDHS] ON  PRIMARY 
( NAME = N'CDHS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\CDHS.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CDHS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\CDHS_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CDHS] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CDHS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CDHS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CDHS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CDHS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CDHS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CDHS] SET ARITHABORT OFF 
GO
ALTER DATABASE [CDHS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CDHS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CDHS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CDHS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CDHS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CDHS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CDHS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CDHS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CDHS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CDHS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CDHS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CDHS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CDHS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CDHS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CDHS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CDHS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CDHS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CDHS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CDHS] SET  MULTI_USER 
GO
ALTER DATABASE [CDHS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CDHS] SET DB_CHAINING OFF 
GO
USE [CDHS]
GO
/****** Object:  Table [dbo].[About_cdhs]    Script Date: 09-Jan-18 2:11:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[About_cdhs](
	[id] [varchar](100) NOT NULL,
	[Title] [varchar](500) NULL,
	[Content_in_Detail] [varchar](1000) NULL,
	[Upload_Title_Image] [varchar](500) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_About_cdhs] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Content_Category]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Content_Category](
	[id] [nchar](100) NOT NULL,
	[Category_Name] [varchar](500) NULL,
	[Category_Description] [varchar](1000) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Content_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Content_Types]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Content_Types](
	[id] [nchar](100) NOT NULL,
	[Type_Name] [varchar](500) NULL,
	[Type_Description] [varchar](1000) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Content_Types] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Diabetes_Content]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Diabetes_Content](
	[id] [nchar](100) NOT NULL,
	[Content_Id] [nvarchar](100) NULL,
	[Category_Name] [varchar](200) NULL,
	[Sponsar_id] [varchar](200) NULL,
	[Content_Title] [varchar](500) NULL,
	[Diabetes_Containt_Details] [varchar](1000) NULL,
	[News_Date] [date] NULL,
	[Priority] [varchar](10) NULL,
	[Approval_Status] [varchar](25) NULL,
	[Asset_Type] [varchar](10) NULL,
	[Upload_Photo] [varchar](500) NULL,
 CONSTRAINT [PK_Diabetes_Content_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Diabetes_Symptoms]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Diabetes_Symptoms](
	[id] [nchar](100) NOT NULL,
	[Diabetes_Symptoms_Title] [varchar](500) NULL,
	[Sponsor_id] [varchar](200) NULL,
	[Short_Description] [varchar](500) NULL,
	[Diabetes_Symptoms_Details] [varchar](1000) NULL,
	[Approval_Status] [varchar](25) NULL,
	[Asset_Type] [varchar](10) NULL,
	[Upload_Title_Image] [varchar](500) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Add_Diabetes_Symptoms] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GP]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GP](
	[id] [nchar](50) NOT NULL,
	[Clinic_Name] [varchar](500) NULL,
	[Contact_Name] [varchar](500) NULL,
	[Mobile_No] [varchar](50) NULL,
	[Email_ID] [varchar](50) NULL,
	[Enter_Password] [varchar](50) NULL,
	[Enter_Confirm_Password] [varchar](50) NULL,
	[Adddress] [varchar](1000) NULL,
	[Subrub] [varchar](500) NULL,
	[State] [varchar](500) NULL,
	[Postcode] [varchar](100) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_GP] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Hyper_Tension_Content]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hyper_Tension_Content](
	[id] [nchar](50) NOT NULL,
	[Content_id] [nchar](50) NULL,
	[Content_Category_Name] [varchar](500) NULL,
	[Sponsers_id] [nchar](50) NULL,
	[Title] [varchar](500) NULL,
	[Hyper_Tension_Containt_Details] [varchar](1000) NULL,
	[Date] [date] NULL,
	[Priority] [varchar](10) NULL,
	[Approval_Status] [varchar](50) NULL,
	[Asset_Type] [varchar](50) NULL,
	[Upload_Photo] [varchar](500) NULL,
 CONSTRAINT [PK_Hyper_Tension_Content] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Hyper_Tension_Symptoms]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Hyper_Tension_Symptoms](
	[id] [nchar](50) NOT NULL,
	[Hyper_Tension_Symptoms_Title] [varchar](500) NULL,
	[Sponser_id] [nchar](50) NULL,
	[Short_Description] [varchar](1000) NULL,
	[Hyper_Tension_Symptoms_Details] [varchar](1000) NULL,
	[Approval_Status] [varchar](50) NULL,
	[Asset_Type] [varchar](50) NULL,
	[Upload_Title_Image] [varchar](500) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Hyper_Tension_Symptoms] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sales_Rep]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sales_Rep](
	[id] [nchar](50) NOT NULL,
	[First_Name] [varchar](200) NULL,
	[Last_Name] [varchar](200) NULL,
	[Email_ID] [varchar](50) NULL,
	[Enter_Password] [varchar](50) NULL,
	[Confirm_Password] [varchar](50) NULL,
	[Mobile_No] [varchar](50) NULL,
	[Subrub] [varchar](500) NULL,
	[State] [varchar](500) NULL,
	[Postcode] [varchar](100) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Sales_Rep] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sponsors]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sponsors](
	[id] [nchar](100) NOT NULL,
	[Company_Name] [varchar](500) NULL,
	[Address] [varchar](1000) NULL,
	[Company_Description] [varchar](1000) NULL,
	[Contact_Number] [varchar](50) NULL,
	[Email_Id] [varchar](50) NULL,
	[Website_URL] [varchar](500) NULL,
	[Upload_Logo] [varchar](500) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Sponsors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[id] [nchar](100) NOT NULL,
	[Name] [varchar](200) NULL,
	[Last _Name] [varchar](200) NULL,
	[Email_ID] [varchar](50) NULL,
	[Enter_Password] [varchar](50) NULL,
	[Confirm_Password] [varchar](50) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Video_Channel]    Script Date: 09-Jan-18 2:11:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Video_Channel](
	[id] [nchar](50) NOT NULL,
	[Title] [varchar](500) NULL,
	[Date] [date] NULL,
	[Sponsor_id] [nchar](50) NULL,
	[Short_Description] [varchar](500) NULL,
	[Video_Link] [nvarchar](255) NULL,
	[Approval_Status] [varchar](50) NULL,
	[Priority] [varchar](10) NULL,
 CONSTRAINT [PK_Video_Channel] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
USE [master]
GO
ALTER DATABASE [CDHS] SET  READ_WRITE 
GO
